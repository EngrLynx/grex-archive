from app import app
from flask import render_template
from flask import g
from flask import request
from flask import jsonify
from flask import abort
import psycopg2
import config

TARGETS_DEFAULT_PAGE_SIZE = 50
DEFAULT_SORT_DIR = "asc"


@app.route("/")
@app.route("/index")
def index():
    print("Test")
    cur = g.db_conn.cursor()

    cur.execute("select count(id) from targets")
    targets_count = cur.fetchone()[0]

    cur.execute("select count(id) from reviewers")
    reviewers_count = cur.fetchone()[0]

    cur.close()
    data = {
        "targets_count": targets_count,
        "reviewers_count": reviewers_count,
        "current_page": "index"
    }

    return render_template("index.html", data=data)


@app.route("/targets")
@app.route("/targets/index.json")
def targets():
    is_ajax_req = \
        request.method == "GET" and \
        request.is_xhr and \
        ".json" in request.url

    limit = TARGETS_DEFAULT_PAGE_SIZE
    offset = 0

    if is_ajax_req:
        limit = request.args.get("length") or limit
        offset = request.args.get("start") or offset

    cur = g.db_conn.cursor()
    cur.execute("""
        select t.id, t.name
        from targets as t
        order by t.id
        limit %s offset %s
    """, [limit, offset])

    trg = cur.fetchall()
    if is_ajax_req:
        fields = ["id", "name"]

        cur.execute("""
            select count(distinct t.id)
            from targets as t
        """)
        trg_total = cur.fetchone()[0]
        cur.close()
        return jsonify({
            "data": [dict(zip(fields, x)) for x in trg],
            "recordsTotal": trg_total,
            "recordsFiltered": trg_total,
            "draw": request.args.get("draw")})
    else:
        cur.close()
        data = {"items": trg, "current_page": "targets"}
        return render_template("target/index.html", data=data)


@app.route("/targets/<idd>")
def show_target(idd):
    cur = g.db_conn.cursor()
    cur.execute("""
        select r.name as "reviewer", rli.body as "review"
        from targets as t
            inner join review_lines as rl on t.id = rl.target_id
            inner join (
                    select rl.target_id, max(rl.inserted_at) as inserted_at
                    from review_lines as rl
                    group by rl.target_id
                    ) as rllpt
                on t.id = rllpt.target_id
                and rl.inserted_at = rllpt.inserted_at
            inner join review_line_items as rli on rl.id = rli.review_line_id
            inner join reviewers as r on rli.reviewer_id = r.id
        where t.id = %s
    """, [idd])

    reviews = cur.fetchall()
    cur.close()
    if reviews is None:
        abort(404)
    else:
        columns = ["reviewer", "review"]
        return render_template("target/show.html", data={
            "current_page": "targets",
            "reviews": [dict(zip(columns, review)) for review in reviews]
        })


@app.before_request
def before_request():
    db = getattr(g, "db_conn", None)
    if db is None:
        g.db_conn = psycopg2.connect(
            host=config.DB_HOST, database=config.DB_NAME,
            user=config.DB_USER, password=config.DB_PASSWORD
        )


@app.teardown_appcontext
def close_connection(exception):
    if hasattr(g, "db_conn") and g.db_conn is not None:
        g.db_conn.close()


@app.errorhandler(404)
def not_found(error):
    return render_template("page_not_found.html"), 404
