function getCities(evt) {
  var slcCities = document.getElementById("slc_cities");
  var slcCtr = document.getElementById("slc_countries");
  var ctrId = slcCtr.options[slcCtr.selectedIndex].value;
  $.ajax({
    url: "/cities/by_country/" + ctrId,
    contentType: "application/json; charset=utf-8",
    dataType: "json",
  }).done(function(data) {
    while (slcCities.childNodes.length > 1) {
      slcCities.removeChild(slcCities.lastChild);
    }

    for (var i = 0; i < data.length; i++) {
      var opt = document.createElement("option");
      var nm = data[i].name;
      opt.innerHTML = nm.charAt(0).toUpperCase() + nm.slice(1);
      opt.setAttribute("value", data[i].id);
      if (i === 0) {
        opt.setAttribute("selected", "true");
      }

      slcCities.appendChild(opt);
    } 
  }).fail(function(jqXHR, textStatus) {
    alert("Something went wrong, please try again: " + jqXHR);
  });
};

var btnSubmit = document.getElementById("btn_submit");
var btnInitText = btnSubmit.value;

document.getElementById("frm_target").onsubmit = function(e) {
  e.preventDefault();

  var xhr = new XMLHttpRequest();
  xhr.open(ajaxReqMethod, ajaxReqUrl, true);
  xhr.setRequestHeader("Content-type", "application/json;charset=UTF-8");

  var slcCntr = document.getElementById("slc_countries");
  var slcCt = document.getElementById("slc_cities");
  var slcTrgTp = document.getElementById("slc_target_types");

  var data = JSON.stringify({
    name: document.getElementById("txt_name").value,
    lrd_parameter: document.getElementById("txt_lrd_parameter").value,
    address: document.getElementById("txt_address").value,
    city_id: slcCt.options[slcCt.selectedIndex].value,
    country_id: slcCntr.options[slcCntr.selectedIndex].value,
    target_type_id: slcTrgTp.options[slcTrgTp.selectedIndex].value});

  btnSubmit.disabled = true;
  btnSubmit.value = "Saving...";
  xhr.send(data);
  xhr.onreadystatechange = function() {
    if (xhr.readyState === 4) {
      btnSubmit.disabled = false;
      btnSubmit.value = btnInitText;
      if (xhr.status === 200) {
        var jsonResp = JSON.parse(xhr.responseText);
        if (jsonResp.status === "ok") {
          alert(jsonResp.message);
          window.location = jsonResp.redirection_path;
        } else {
          alert("Something went wrong: " + jsonResp.data);
        }
      } else {
        alert("Something went wrong on the server, try again later");
      }
    }
  };
};
