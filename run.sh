#!/bin/bash

BIND='0.0.0.0:8080'
WORKERS=4

gunicorn -w ${WORKERS} -b ${BIND} app:app
